##基于FormBuild的表单设计器
>项目源码:https://codeload.github.com/kevinchappell/formBuilder/zip/master
>预览地址:customerForm.html<br/>
>css/customer.css为表单设计器的自定义样式文件<br/>
>assets/js/demo.js为实际处理的js文件<br/>
>assets/js/form-render.min.js为json解析文件<br/>
>assets/js/form-builder.min.js为表单设计器<br/>
>引入MUI手机端样式
