#叮叮相关整理
###南京中程智汇信息科技有限公司
>CorpId:dinge55e70a2af8a06dc35c2f4657eb6378f	
>CorpSecret:NAZ3UhyqPK2ftGtTRAT-E9HUnRHqD_2gfV6mWBjAFxP9j5GsXMdQeyqQQEh9uYhA	

#####获取Access_Token:
>获取Access_Token:GET
https://oapi.dingtalk.com/gettoken?corpid=dinge55e70a2af8a06dc35c2f4657eb6378f&corpsecret=NAZ3UhyqPK2ftGtTRAT-E9HUnRHqD_2gfV6mWBjAFxP9j5GsXMdQeyqQQEh9uYhA
```
{
  "access_token": "793ef779008133839bb19713581b5301",
  "errcode": 0,
  "errmsg": "ok"
}
```

#####部门管理
* 获取部门列表

>Https请求方式: GET 
>https://oapi.dingtalk.com/department/list?access_token=ACCESS_TOKEN

```
{
  "department": [
    {
      "autoAddUser": false,
      "createDeptGroup": false,
      "id": 1,
      "name": "南京中程智汇信息科技有限公司"
    }
  ],
  "errcode": 0,
  "errmsg": "ok"
}
```
* 获取部门详情

>Https请求方式: GET
>https://oapi.dingtalk.com/department/get?access_token=ACCESS_TOKEN&id=2

```
{
  "autoAddUser": false,
  "createDeptGroup": false,
  "deptHiding": false,
  "deptManagerUseridList": "",
  "deptPerimits": "",
  "errcode": 0,
  "errmsg": "ok",
  "id": 1,
  "name": "南京中程智汇信息科技有限公司",
  "order": 0,
  "orgDeptOwner": "manager1789",
  "outerDept": false,
  "outerPermitDepts": "",
  "outerPermitUsers": "",
  "userPerimits": ""
}
```
* 创建部门
>Https请求方式: POST
>https://oapi.dingtalk.com/department/create?access_token=ACCESS_TOKEN
>请求包结构体:
```
{
    "name": "钉钉事业部",
    "parentid": "1",
    "order": "1",
    "createDeptGroup": true,
    "deptHiding" : true,
    "deptPerimits" : "3|4",
    "userPerimits" : "userid1|userid2",
    "outerDept" : true,
    "outerPermitDepts" : "1|2",
    "outerPermitUsers" : "userid3|userid4"
}
```
>返回数据

```
{
    "errcode": 0,
    "errmsg": "created",
    "id": 2
}
```
* 更新部门

>Https请求方式: POST
>https://oapi.dingtalk.com/department/update?access_token=ACCESS_TOKEN

请求包结构体
```
{
    "name": "钉钉事业部",
    "parentid": "1",
    "order": "1",
    "id": "1",
    "createDeptGroup": true,
    "autoAddUser": true,
    "deptManagerUseridList": "manager1111|2222",
    "deptHiding" : true,
    "deptPerimits" : "3|4",
    "userPerimits" : "userid1|userid2",
    "outerDept" : true,
    "outerPermitDepts" : "1|2",
    "outerPermitUsers" : "userid3|userid4",
    "orgDeptOwner": "manager1111"
}
```
>返回数据

```
{
    "errcode": 0,
    "errmsg": "updated"
}
```
* 删除部门

>Https请求方式: GET
>https://oapi.dingtalk.com/department/delete?access_token=ACCESS_TOKEN&id=ID

返回结果

```
{
    "errcode": 0,
    "errmsg": "deleted"
}
```


#####用户管理

* 获取部门成员
>Https请求方式: GET
>https://oapi.dingtalk.com/user/simplelist?access_token=ACCESS_TOKEN&department_id=1

返回:

```
{
  "errcode": 0,
  "errmsg": "ok",
  "userlist": [
    {
      "name": "顾长远",
      "userid": "066513312238738747"
    },
    {
      "name": "沙正波",
      "userid": "082263205727596856"
    },
    {
      "name": "沈笑云",
      "userid": "manager1789"
    },
    {
      "name": "司雨",
      "userid": "0715620152705008"
    },
    {
      "name": "王天阳",
      "userid": "0467634229171445"
    }
  ]
}

```
* 获取成员详情

>Https请求方式: GET
>https://oapi.dingtalk.com/user/get?access_token=ACCESS_TOKEN&userid=zhangsan

返回结果:
```
{
  "active": true,
  "avatar": "",
  "department": [
    1
  ],
  "dingId": "$:LWCP_v1:$Ykh4FKvVvcM13cGCKxmb8w==",
  "email": "",
  "errcode": 0,
  "errmsg": "ok",
  "isAdmin": true,
  "isBoss": false,
  "isHide": false,
  "isLeaderInDepts": "{1:false}",
  "isSenior": false,
  "jobnumber": "",
  "mobile": "15305166364",
  "name": "沙正波",
  "openId": "34Ot4rCQmTqMBfsgyvYii6AiEiE",
  "orderInDepts": "{1:36087261572368448}",
  "position": "",
  "remark": "",
  "stateCode": "86",
  "tel": "",
  "unionid": "34Ot4rCQmTqMBfsgyvYii6AiEiE",
  "userid": "082263205727596856",
  "workPlace": ""
}

```

* 根据unionid获取成员的userid
>Https请求方式: GET
>https://oapi.dingtalk.com/user/getUseridByUnionid?access_token=ACCESS_TOKEN&unionid=xxxxxx

返回结果:

```
{
  "contactType": 0,
  "errcode": 0,
  "errmsg": "ok",
  "userid": "082263205727596856"
}
```

* 创建成员

>Https请求方式: POST
>https://oapi.dingtalk.com/user/create?access_token=ACCESS_TOKEN

请求包结构体

```
{
    "userid": "zhangsan",
    "name": "张三",
    "orderInDepts" : "{1:10, 2:20}",
    "department": [1, 2],
    "position": "产品经理",
    "mobile": "15913215421",
    "tel" : "010-123333",
    "workPlace" :"",
    "remark" : "",
    "email": "zhangsan@gzdev.com",
    "jobnumber": "111111",
    "isHide": false,
    "isSenior": false,
    "extattr": {
                "爱好":"旅游",
                "年龄":"24"
                }
}
```

返回结果:

```
{
    "errcode": 0,
    "errmsg": "created",
    "userid": "dedwefewfwe1231"
}
```

* 更新成员

>Https请求方式: POST
>https://oapi.dingtalk.com/user/update?access_token=ACCESS_TOKEN

```
{
    "userid": "zhangsan",
    "name": "张三",
    "department": [1, 2],
    "orderInDepts": "{1:10}",
    "position": "产品经理",
    "mobile": "15913215421",
    "tel" : "010-123333",
    "workPlace" :"",
    "remark" : "",
    "email": "zhangsan@gzdev.com",
    "jobnumber": "111111",
    "isHide": false,
    "isSenior": false,
    "extattr": {
                "爱好":"旅游",
                "年龄":"24"
                }
}
```

返回结果:

```
{
    "errcode": 0,
    "errmsg": "updated"
}
```
* 删除成员

>Https请求方式: GET
>https://oapi.dingtalk.com/user/delete?access_token=ACCESS_TOKEN&userid=ID

```
{
    "errcode": 0,
    "errmsg": "deleted"
}
```

* 获取管理员列表

>Https请求方式: GET
>https://oapi.dingtalk.com/user/get_admin?access_token=ACCESS_TOKEN

```
{
  "adminList": [
    {
      "sys_level": 1,
      "userid": "0467634229171445"
    },
    {
      "sys_level": 2,
      "userid": "082263205727596856"
    },
    {
      "sys_level": 2,
      "userid": "0715620152705008"
    },
    {
      "sys_level": 2,
      "userid": "066513312238738747"
    }
  ],
  "errcode": 0,
  "errmsg": "ok"
}

```

* 获取通讯录权限(获取当前token能够读取到的通讯录部门及人员范围...)

>Https请求方式: GET
>https://oapi.dingtalk.com/auth/scopes?access_token=ACCESS_TOKEN

返回結果:

```
{
    "errcode": 0,
    "errmsg": "created",
    "auth_user_field": ["name","email"],
    "auth_org_scopes":{
        "authed_dept":[1,2,3],
        "authed_user":["user1","user"]
    }
}
```

