package com.snio.dingding.activiti;

import java.io.IOException;
import java.util.List;

import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-datasource.xml",
		"classpath:spring/applicationContext-activti.xml", "classpath:spring/applicationContext-transaction.xml" })
public class ActivitiModelTest {

	@Autowired
	private RepositoryService repositoryService;
	private Gson gson = new Gson();

	/**
	 * 查看已经定义的流程modeler
	 */
	@Test
	public void getProcess() {
		List<Model> list = repositoryService.createModelQuery().orderByCreateTime().desc().list();
		for (Model model : list) {
			System.err.println(gson.toJson(model));
		}

	}

	/**
	 * 根据modelId删除流程模型
	 */
	@Test
	public void delModelById() {
		String modelId = "1";
		repositoryService.deleteModel(modelId);
	}

	/**
	 * 部署流程
	 * 
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@Test
	public void deploymentModel() throws JsonProcessingException, IOException {
		String modelId = "2501";
		Model modelData = repositoryService.getModel(modelId);
		ObjectNode modelNode = (ObjectNode) new ObjectMapper()
				.readTree(repositoryService.getModelEditorSource(modelData.getId()));
		byte[] bpmnBytes = null;
		BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
		bpmnBytes = new BpmnXMLConverter().convertToXML(model);
		String processName = modelData.getName() + ".bpmn20.xml";
		Deployment deployment = repositoryService.createDeployment().name(modelData.getName())
				.addString(processName, new String(bpmnBytes, "utf-8")).deploy();
		System.err.println("流程部署的ID:" + deployment.getId());
		System.err.println("流程部署的名称:" + deployment.getName());
	}

	/**
	 * 已经部署流程列表
	 */
	@Test
	public void deployList() {
		List<Deployment> list = repositoryService.createDeploymentQuery().list();
		for (Deployment deployment : list) {
			System.err.println(gson.toJson(deployment));
		}
		String deploymentId = "5001";
		List<String> delstrList = repositoryService.getDeploymentResourceNames(deploymentId);
		for (String string : delstrList) {
			System.err.println(string);
		}
		// 查看流程定义
		List<ProcessDefinition> processList = repositoryService.createProcessDefinitionQuery().list();
		for (ProcessDefinition processDefinition : processList) {
			System.err.println(processDefinition.getId());
		}
	}

	@Autowired
	private RuntimeService runtimeService;

	/**
	 * 启动流程实例
	 */
	@Test
	public void getRunService() {
		// 启动流程实例
		String processDefinitionKey = "process";
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey);
		System.err.println("流程实例Id"+processInstance.getId());
		System.err.println("流程实例名称:"+processInstance.getName());
		System.err.println(gson.toJson(processInstance));
	}

}
