package com.snio.dingding.user;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.snio.dingding.entity.User;
import com.snio.dingding.entity.UserInfo;
import com.snio.dingding.service.IUserService;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations = { "classpath:spring/applicationContext-datasource.xml",
		"classpath:spring/applicationContext-activti.xml", "classpath:spring/applicationContext-transaction.xml" })
public class UserTest {

	@Autowired
	private IUserService iUserService;
	private Gson gson = new Gson();

	/**
	 * 根据用户工号查询用户信息
	 */
	@Test
	public void getUserByAccount() {
		List<User> users = iUserService.getUserByAccount("1231231132");
		for (User user : users) {
			System.err.println(gson.toJson(user));
		}
	}
	/**
	 * 根据用户id查询用户详情
	 */
	@Test
	public void getUserByUserId(){
		User user=iUserService.getUserByUserId("a28813da-40d0-434a-be96-dbd6a497beb1");
		System.err.println(gson.toJson(user));
	}
	/**
	 * 添加用户信息
	 */
	@Test
	public void addUserAccount() {
		String userId = iUserService.addUser(new User(UUID.randomUUID().toString(), "sha", "123", "18112485785",
				"1047812462@qq.com", new Date(), 1, "http://a.png", "南京南钢", "1", "1231212", "浦口", "29", "1231231132"));
		System.err.println(userId);
	}
	@Test
	public void getUserInfoByUserId(){
		User u=new User();
//		u.setUserid("d70aac9f-78f9-413c-bef4-8779d5a76626");
		u.setJobnumber("1231231132");
		List<UserInfo> userInfos=iUserService.getUserInfoByUserName(u);
		for (UserInfo userInfo : userInfos) {
			System.out.println(gson.toJson(userInfo));
		}
	}
	
	

}
