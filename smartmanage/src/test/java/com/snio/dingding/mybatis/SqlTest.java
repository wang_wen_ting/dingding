package com.snio.dingding.mybatis;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import com.snio.dingding.entity.User;
import com.snio.dingding.mapper.UserMapper;

public class SqlTest {

	@Test
	public void getSql() throws IOException {
		String resource = "mybatis/mybatisconfigtest.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		SqlSession sqlSession = sqlSessionFactory.openSession();
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		List<User> userList=userMapper.selectByExample(null);
		System.out.println(userList.size()+"A");
	}
	

}
