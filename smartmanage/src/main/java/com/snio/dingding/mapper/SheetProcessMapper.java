package com.snio.dingding.mapper;

import com.snio.dingding.entity.SheetProcess;
import com.snio.dingding.entity.SheetProcessExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SheetProcessMapper {
    long countByExample(SheetProcessExample example);

    int deleteByExample(SheetProcessExample example);

    int deleteByPrimaryKey(String sheetprocessid);

    int insert(SheetProcess record);

    int insertSelective(SheetProcess record);

    List<SheetProcess> selectByExample(SheetProcessExample example);

    SheetProcess selectByPrimaryKey(String sheetprocessid);

    int updateByExampleSelective(@Param("record") SheetProcess record, @Param("example") SheetProcessExample example);

    int updateByExample(@Param("record") SheetProcess record, @Param("example") SheetProcessExample example);

    int updateByPrimaryKeySelective(SheetProcess record);

    int updateByPrimaryKey(SheetProcess record);
}