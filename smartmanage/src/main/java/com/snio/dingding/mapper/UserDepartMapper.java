package com.snio.dingding.mapper;

import com.snio.dingding.entity.UserDepart;
import com.snio.dingding.entity.UserDepartExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserDepartMapper {
    long countByExample(UserDepartExample example);

    int deleteByExample(UserDepartExample example);

    int deleteByPrimaryKey(String userDepartId);

    int insert(UserDepart record);

    int insertSelective(UserDepart record);

    List<UserDepart> selectByExample(UserDepartExample example);

    UserDepart selectByPrimaryKey(String userDepartId);

    int updateByExampleSelective(@Param("record") UserDepart record, @Param("example") UserDepartExample example);

    int updateByExample(@Param("record") UserDepart record, @Param("example") UserDepartExample example);

    int updateByPrimaryKeySelective(UserDepart record);

    int updateByPrimaryKey(UserDepart record);
}