package com.snio.dingding.mapper;

import com.snio.dingding.entity.FormDetail;
import com.snio.dingding.entity.FormDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FormDetailMapper {
    long countByExample(FormDetailExample example);

    int deleteByExample(FormDetailExample example);

    int deleteByPrimaryKey(String id);

    int insert(FormDetail record);

    int insertSelective(FormDetail record);

    List<FormDetail> selectByExampleWithBLOBs(FormDetailExample example);

    List<FormDetail> selectByExample(FormDetailExample example);

    FormDetail selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") FormDetail record, @Param("example") FormDetailExample example);

    int updateByExampleWithBLOBs(@Param("record") FormDetail record, @Param("example") FormDetailExample example);

    int updateByExample(@Param("record") FormDetail record, @Param("example") FormDetailExample example);

    int updateByPrimaryKeySelective(FormDetail record);

    int updateByPrimaryKeyWithBLOBs(FormDetail record);

    int updateByPrimaryKey(FormDetail record);
}