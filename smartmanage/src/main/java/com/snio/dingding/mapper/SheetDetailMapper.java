package com.snio.dingding.mapper;

import com.snio.dingding.entity.SheetDetail;
import com.snio.dingding.entity.SheetDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SheetDetailMapper {
    long countByExample(SheetDetailExample example);

    int deleteByExample(SheetDetailExample example);

    int deleteByPrimaryKey(String sheetdetailid);

    int insert(SheetDetail record);

    int insertSelective(SheetDetail record);

    List<SheetDetail> selectByExample(SheetDetailExample example);

    SheetDetail selectByPrimaryKey(String sheetdetailid);

    int updateByExampleSelective(@Param("record") SheetDetail record, @Param("example") SheetDetailExample example);

    int updateByExample(@Param("record") SheetDetail record, @Param("example") SheetDetailExample example);

    int updateByPrimaryKeySelective(SheetDetail record);

    int updateByPrimaryKey(SheetDetail record);
}