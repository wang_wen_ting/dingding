package com.snio.dingding.mapper;

import com.snio.dingding.entity.SheetInfo;
import com.snio.dingding.entity.SheetInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SheetInfoMapper {
    long countByExample(SheetInfoExample example);

    int deleteByExample(SheetInfoExample example);

    int deleteByPrimaryKey(String sheetid);

    int insert(SheetInfo record);

    int insertSelective(SheetInfo record);

    List<SheetInfo> selectByExample(SheetInfoExample example);

    SheetInfo selectByPrimaryKey(String sheetid);

    int updateByExampleSelective(@Param("record") SheetInfo record, @Param("example") SheetInfoExample example);

    int updateByExample(@Param("record") SheetInfo record, @Param("example") SheetInfoExample example);

    int updateByPrimaryKeySelective(SheetInfo record);

    int updateByPrimaryKey(SheetInfo record);
}