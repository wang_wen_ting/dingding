package com.snio.dingding.mapper;

import java.util.List;

import com.snio.dingding.entity.User;
import com.snio.dingding.entity.UserInfo;

public interface UserInfoMapper {

	List<UserInfo> getUserInfo(User user);
}
