package com.snio.dingding.mapper;

import com.snio.dingding.entity.FormInfo;
import com.snio.dingding.entity.FormInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FormInfoMapper {
    long countByExample(FormInfoExample example);

    int deleteByExample(FormInfoExample example);

    int deleteByPrimaryKey(String formid);

    int insert(FormInfo record);

    int insertSelective(FormInfo record);

    List<FormInfo> selectByExample(FormInfoExample example);

    FormInfo selectByPrimaryKey(String formid);

    int updateByExampleSelective(@Param("record") FormInfo record, @Param("example") FormInfoExample example);

    int updateByExample(@Param("record") FormInfo record, @Param("example") FormInfoExample example);

    int updateByPrimaryKeySelective(FormInfo record);

    int updateByPrimaryKey(FormInfo record);
}