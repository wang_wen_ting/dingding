package com.snio.dingding.entity;

import java.util.ArrayList;
import java.util.List;

public class SheetProcessExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SheetProcessExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSheetprocessidIsNull() {
            addCriterion("sheetProcessId is null");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidIsNotNull() {
            addCriterion("sheetProcessId is not null");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidEqualTo(String value) {
            addCriterion("sheetProcessId =", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidNotEqualTo(String value) {
            addCriterion("sheetProcessId <>", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidGreaterThan(String value) {
            addCriterion("sheetProcessId >", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidGreaterThanOrEqualTo(String value) {
            addCriterion("sheetProcessId >=", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidLessThan(String value) {
            addCriterion("sheetProcessId <", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidLessThanOrEqualTo(String value) {
            addCriterion("sheetProcessId <=", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidLike(String value) {
            addCriterion("sheetProcessId like", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidNotLike(String value) {
            addCriterion("sheetProcessId not like", value, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidIn(List<String> values) {
            addCriterion("sheetProcessId in", values, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidNotIn(List<String> values) {
            addCriterion("sheetProcessId not in", values, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidBetween(String value1, String value2) {
            addCriterion("sheetProcessId between", value1, value2, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetprocessidNotBetween(String value1, String value2) {
            addCriterion("sheetProcessId not between", value1, value2, "sheetprocessid");
            return (Criteria) this;
        }

        public Criteria andSheetidIsNull() {
            addCriterion("sheetID is null");
            return (Criteria) this;
        }

        public Criteria andSheetidIsNotNull() {
            addCriterion("sheetID is not null");
            return (Criteria) this;
        }

        public Criteria andSheetidEqualTo(String value) {
            addCriterion("sheetID =", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidNotEqualTo(String value) {
            addCriterion("sheetID <>", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidGreaterThan(String value) {
            addCriterion("sheetID >", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidGreaterThanOrEqualTo(String value) {
            addCriterion("sheetID >=", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidLessThan(String value) {
            addCriterion("sheetID <", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidLessThanOrEqualTo(String value) {
            addCriterion("sheetID <=", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidLike(String value) {
            addCriterion("sheetID like", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidNotLike(String value) {
            addCriterion("sheetID not like", value, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidIn(List<String> values) {
            addCriterion("sheetID in", values, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidNotIn(List<String> values) {
            addCriterion("sheetID not in", values, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidBetween(String value1, String value2) {
            addCriterion("sheetID between", value1, value2, "sheetid");
            return (Criteria) this;
        }

        public Criteria andSheetidNotBetween(String value1, String value2) {
            addCriterion("sheetID not between", value1, value2, "sheetid");
            return (Criteria) this;
        }

        public Criteria andProcessidIsNull() {
            addCriterion("processId is null");
            return (Criteria) this;
        }

        public Criteria andProcessidIsNotNull() {
            addCriterion("processId is not null");
            return (Criteria) this;
        }

        public Criteria andProcessidEqualTo(String value) {
            addCriterion("processId =", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidNotEqualTo(String value) {
            addCriterion("processId <>", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidGreaterThan(String value) {
            addCriterion("processId >", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidGreaterThanOrEqualTo(String value) {
            addCriterion("processId >=", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidLessThan(String value) {
            addCriterion("processId <", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidLessThanOrEqualTo(String value) {
            addCriterion("processId <=", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidLike(String value) {
            addCriterion("processId like", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidNotLike(String value) {
            addCriterion("processId not like", value, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidIn(List<String> values) {
            addCriterion("processId in", values, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidNotIn(List<String> values) {
            addCriterion("processId not in", values, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidBetween(String value1, String value2) {
            addCriterion("processId between", value1, value2, "processid");
            return (Criteria) this;
        }

        public Criteria andProcessidNotBetween(String value1, String value2) {
            addCriterion("processId not between", value1, value2, "processid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}