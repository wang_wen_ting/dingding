package com.snio.dingding.entity;

import java.util.ArrayList;
import java.util.List;

public class UserDepartExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserDepartExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUserDepartIdIsNull() {
            addCriterion("user_depart_id is null");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdIsNotNull() {
            addCriterion("user_depart_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdEqualTo(String value) {
            addCriterion("user_depart_id =", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdNotEqualTo(String value) {
            addCriterion("user_depart_id <>", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdGreaterThan(String value) {
            addCriterion("user_depart_id >", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdGreaterThanOrEqualTo(String value) {
            addCriterion("user_depart_id >=", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdLessThan(String value) {
            addCriterion("user_depart_id <", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdLessThanOrEqualTo(String value) {
            addCriterion("user_depart_id <=", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdLike(String value) {
            addCriterion("user_depart_id like", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdNotLike(String value) {
            addCriterion("user_depart_id not like", value, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdIn(List<String> values) {
            addCriterion("user_depart_id in", values, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdNotIn(List<String> values) {
            addCriterion("user_depart_id not in", values, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdBetween(String value1, String value2) {
            addCriterion("user_depart_id between", value1, value2, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andUserDepartIdNotBetween(String value1, String value2) {
            addCriterion("user_depart_id not between", value1, value2, "userDepartId");
            return (Criteria) this;
        }

        public Criteria andIsleaderIsNull() {
            addCriterion("isLeader is null");
            return (Criteria) this;
        }

        public Criteria andIsleaderIsNotNull() {
            addCriterion("isLeader is not null");
            return (Criteria) this;
        }

        public Criteria andIsleaderEqualTo(String value) {
            addCriterion("isLeader =", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderNotEqualTo(String value) {
            addCriterion("isLeader <>", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderGreaterThan(String value) {
            addCriterion("isLeader >", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderGreaterThanOrEqualTo(String value) {
            addCriterion("isLeader >=", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderLessThan(String value) {
            addCriterion("isLeader <", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderLessThanOrEqualTo(String value) {
            addCriterion("isLeader <=", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderLike(String value) {
            addCriterion("isLeader like", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderNotLike(String value) {
            addCriterion("isLeader not like", value, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderIn(List<String> values) {
            addCriterion("isLeader in", values, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderNotIn(List<String> values) {
            addCriterion("isLeader not in", values, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderBetween(String value1, String value2) {
            addCriterion("isLeader between", value1, value2, "isleader");
            return (Criteria) this;
        }

        public Criteria andIsleaderNotBetween(String value1, String value2) {
            addCriterion("isLeader not between", value1, value2, "isleader");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("userId =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("userId <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("userId >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("userId >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("userId <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("userId <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("userId like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("userId not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("userId in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("userId not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("userId between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("userId not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andDepartidIsNull() {
            addCriterion("departId is null");
            return (Criteria) this;
        }

        public Criteria andDepartidIsNotNull() {
            addCriterion("departId is not null");
            return (Criteria) this;
        }

        public Criteria andDepartidEqualTo(String value) {
            addCriterion("departId =", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidNotEqualTo(String value) {
            addCriterion("departId <>", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidGreaterThan(String value) {
            addCriterion("departId >", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidGreaterThanOrEqualTo(String value) {
            addCriterion("departId >=", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidLessThan(String value) {
            addCriterion("departId <", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidLessThanOrEqualTo(String value) {
            addCriterion("departId <=", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidLike(String value) {
            addCriterion("departId like", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidNotLike(String value) {
            addCriterion("departId not like", value, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidIn(List<String> values) {
            addCriterion("departId in", values, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidNotIn(List<String> values) {
            addCriterion("departId not in", values, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidBetween(String value1, String value2) {
            addCriterion("departId between", value1, value2, "departid");
            return (Criteria) this;
        }

        public Criteria andDepartidNotBetween(String value1, String value2) {
            addCriterion("departId not between", value1, value2, "departid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}