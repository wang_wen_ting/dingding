package com.snio.dingding.entity;

public class FormDetail {
    private String id;

    private String formid;

    private String sheetdetailid;

    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFormid() {
        return formid;
    }

    public void setFormid(String formid) {
        this.formid = formid == null ? null : formid.trim();
    }

    public String getSheetdetailid() {
        return sheetdetailid;
    }

    public void setSheetdetailid(String sheetdetailid) {
        this.sheetdetailid = sheetdetailid == null ? null : sheetdetailid.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}