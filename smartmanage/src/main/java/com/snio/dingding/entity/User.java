package com.snio.dingding.entity;

import java.util.Date;

public class User {
    private String userid;

    private String username;

    private String password;

    private String phone;

    private String email;

    private Date birthtime;

    private Integer sex;

    private String resid;

    private String address;

    private String state;

    private String duserid;

    private String workplace;

    private String orderindeparts;

    private String jobnumber;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Date getBirthtime() {
        return birthtime;
    }

    public void setBirthtime(Date birthtime) {
        this.birthtime = birthtime;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid == null ? null : resid.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getDuserid() {
        return duserid;
    }

    public void setDuserid(String duserid) {
        this.duserid = duserid == null ? null : duserid.trim();
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace == null ? null : workplace.trim();
    }

    public String getOrderindeparts() {
        return orderindeparts;
    }

    public void setOrderindeparts(String orderindeparts) {
        this.orderindeparts = orderindeparts == null ? null : orderindeparts.trim();
    }

    public String getJobnumber() {
        return jobnumber;
    }

    public void setJobnumber(String jobnumber) {
        this.jobnumber = jobnumber == null ? null : jobnumber.trim();
    }
    
    //自定义构造方法
    public User() {
	}

	public User(String userid, String username, String password, String phone, String email, Date birthtime,
			Integer sex, String resid, String address, String state, String duserid, String workplace,
			String orderindeparts, String jobnumber) {
		super();
		this.userid = userid;
		this.username = username;
		this.password = password;
		this.phone = phone;
		this.email = email;
		this.birthtime = birthtime;
		this.sex = sex;
		this.resid = resid;
		this.address = address;
		this.state = state;
		this.duserid = duserid;
		this.workplace = workplace;
		this.orderindeparts = orderindeparts;
		this.jobnumber = jobnumber;
	}
    
    
    
}