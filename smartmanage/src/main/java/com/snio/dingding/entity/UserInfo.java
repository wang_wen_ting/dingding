package com.snio.dingding.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户详情信息扩展
 * 
 * @author CrazyShaQiuShi
 *
 */
public class UserInfo extends User {
	private List<Department> departments = new ArrayList<>();
	private List<Role> roles = new ArrayList<>();
	private List<Permission> permissions = new ArrayList<>();
	
	public List<Department> getDepartments() {
		return departments;
	}
	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	
}
