package com.snio.dingding.entity;

public class SheetProcess {
    private String sheetprocessid;

    private String sheetid;

    private String processid;

    public String getSheetprocessid() {
        return sheetprocessid;
    }

    public void setSheetprocessid(String sheetprocessid) {
        this.sheetprocessid = sheetprocessid == null ? null : sheetprocessid.trim();
    }

    public String getSheetid() {
        return sheetid;
    }

    public void setSheetid(String sheetid) {
        this.sheetid = sheetid == null ? null : sheetid.trim();
    }

    public String getProcessid() {
        return processid;
    }

    public void setProcessid(String processid) {
        this.processid = processid == null ? null : processid.trim();
    }
}