package com.snio.dingding.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/form")
public class FormBuilderController {

	@RequestMapping("/save")
	public Map<String, Object> saveForm(String formstr) {
		Map<String, Object> map = new HashMap<>();
		map.put("result", true);
		return map;
	}

}
