package com.snio.dingding.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.snio.dingding.entity.User;
import com.snio.dingding.service.IUserService;
/**
 * 版权所有：南京中程智慧科技有限公司
 * 编写日期：2017年2月13日-上午9:40:06
 * @Version：1.0 
 * @author:shazhengbo   
 */

@Controller
public class HelloController {
	
	@Autowired
	private IUserService iUserService; 
	@RequestMapping("/a")
	public String index(){
		return "index";
	}
	@RequestMapping("/b")
	@ResponseBody
	public List<User> getUList(){
		return iUserService.getUserList();
	}

}
