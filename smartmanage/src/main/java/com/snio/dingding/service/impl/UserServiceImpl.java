package com.snio.dingding.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snio.dingding.entity.User;
import com.snio.dingding.entity.UserExample;
import com.snio.dingding.entity.UserInfo;
import com.snio.dingding.mapper.UserInfoMapper;
import com.snio.dingding.mapper.UserMapper;
import com.snio.dingding.service.IUserService;

@Service("iUserService")
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Override
	public List<User> getUserList() {
		return userMapper.selectByExample(null);
	}

	@Override
	public List<User> getUserByAccount(String jobNumber) {
		UserExample example = new UserExample();
		example.createCriteria().andJobnumberEqualTo(jobNumber);
		return userMapper.selectByExample(example);
	}

	/**
	 * 新增用户信息
	 */
	@Override
	public String addUser(User user) {
		try {
			userMapper.insert(user);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return user.getUserid();
	}

	@Override
	public User getUserByUserId(String userId) {
		
		return userMapper.selectByPrimaryKey(userId);
	}

	@Override
	public List<UserInfo> getUserInfoByUserName(User user) {
		
		return userInfoMapper.getUserInfo(user);
	}

}
