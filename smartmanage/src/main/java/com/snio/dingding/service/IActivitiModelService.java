package com.snio.dingding.service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.runtime.ProcessInstance;

/**
 * @copyright:南京中程智慧科技有限公司
 * @createTime:2017年2月13日-上午10:43:11
 * @Version:1.0
 * @author:shazhengbo
 * @description:工作流操作相关
 */

public interface IActivitiModelService {

	/**
	 * 流程Model列表
	 * 
	 * @return
	 */
	List<Model> getModelList();

	/**
	 * 根据modelId删除model
	 * 
	 * @param modelId
	 * @return
	 */
	Map<String, Object> delModel(String modelId);

	/**
	 * 根据modelId部署流程
	 * 
	 * @param modelId
	 * @return
	 */
	Map<String, Object> deploymentModel(String modelId);

	/**
	 * 查看所有部署的流程
	 * 
	 * @return
	 */
	List<Deployment> deploymentList();

	/**
	 * 启动流程通过key
	 * 
	 * @param key
	 * @return map
	 */
	ProcessInstance startprocessInstanceByKey(String key);

}
