package com.snio.dingding.service;

import java.util.List;

import com.snio.dingding.entity.User;
import com.snio.dingding.entity.UserInfo;

public interface IUserService {

	List<User> getUserList();

	/**
	 * * 根据员工号查询用户信息
	 * 
	 * @param jobNumber
	 * @return List<User>
	 */
	List<User> getUserByAccount(String jobNumber);
	/**
	 * 根据用户id查询用户详情信息
	 * @param userId
	 * @return User
	 */
	User getUserByUserId(String userId);
	
	/**
	 * 新增用户信息
	 * @param user
	 * @return uuid
	 */
	String addUser(User user);
	
	List<UserInfo> getUserInfoByUserName(User user);
	
	

}
