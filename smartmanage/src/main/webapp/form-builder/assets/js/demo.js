jQuery(document).ready(function($) {
	var buildWrap = document.querySelector('.build-wrap'),
		renderWrap = document.querySelector('.render-wrap'),
		editBtn = document.getElementById('edit-form'),
		formData = window.sessionStorage.getItem('formData'),
		editing = true,
		fbOptions = {
			dataType: 'json'
		};

	if(formData) {
		fbOptions.formData = JSON.parse(formData);
	}

	var toggleEdit = function() {
		document.body.classList.toggle('form-rendered', editing);
		editing = !editing;
	};

	var formBuilder = $(buildWrap).formBuilder(fbOptions).data('formBuilder');

	$('.form-builder-save').click(function() {
		console.log(editing)
		displayView();
		toggleEdit();
		$(renderWrap).formRender({
			dataType: 'json',
			formData: formBuilder.formData
		});
		var myjson = eval("(" + formBuilder.formData + ")")
		window.sessionStorage.setItem('formData', JSON.stringify(formBuilder.formData));
		//添加返回栏目
	});

	editBtn.onclick = function() {
		hiddenView();
		toggleEdit();
	};
	//显示预览手机
	var displayView=function(){
		$(".PhoneBox").css({"display":"block"})
	}
	var hiddenView=function(){
		$(".PhoneBox").css({"display":"none"})
	}
});