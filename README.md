#钉钉企业版微应用
SpringMvc+Mybatis+Activiti+Shiro

数据源监控地址:http://localhost:8686/smartmanage/druid/datasource.html
流程定义入口地址:http://localhost:8686/smartmanage/model/create?name=Demo&key=DemoProcess&description=test
逆向工程执行命令:mybatis-generator:generate
逆向工程配置文件:/src/main/resources/mybatis/mybatis-generator.xml

2017-02-10:新增相关设计文档、原型设计文档、静态页面等
新增SQL查询语句
DROP PROCEDURE IF EXISTS getUserInfo;
CREATE PROCEDURE getUserInfo(IN uid VARCHAR(50))

	BEGIN
			SET @Uid=uid;
			SELECT 
				u.userId AS userId,
				u.userName AS userName,
				u.passWord AS passWord,
				u.phone AS phone,
				u.email AS email,
				u.birthTime AS birthTime,
				u.sex AS sex,
				u.resId AS UresId,
				u.address AS address,
				u.state AS state,
				u.dUserId AS dUserId,
				u.workPlace AS workPlace,
				u.orderInDeparts AS orderInDeparts,
				u.jobNumber AS jobNumber,
				r.roleId AS roleId,
				r.name AS Rname,
				r.description AS Rdescription,
				r.roleSign AS roleSign,
				r.parentId AS RparentId,
				r.resId AS RresId,
				UD.departId AS departId ,
				D.description AS Ddescription,
				D.name AS Dname,
				D.phone AS Dphone,
				D.parentId AS DparentId,
				D.tel AS Dtel,
				RP.permissionId AS RPpermissionId,
				P.name AS Pname,
				P.description AS Pdescription,
				P.permissionOrder AS permissionOrder,
				P.resourcePath AS PresourcePath
				FROM t_user  AS u LEFT JOIN t_user_role AS UR ON u.userId=UR.userId
				LEFT JOIN t_role AS r ON ur.roleId=r.roleId 
				LEFT JOIN t_user_depart AS UD ON u.userId=UD.userId 
				LEFT JOIN t_department AS D ON UD.departId=D.departId 
				LEFT JOIN t_role_permission AS RP ON R.roleId=RP.roleId 
				LEFT JOIN t_permission AS P ON RP.permissionId=P.permissionId WHERE U.userId=@Uid;
	END