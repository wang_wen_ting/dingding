/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017/2/12 10:40:16                           */
/*==============================================================*/


drop table if exists t_department;

drop table if exists t_form_detail;

drop table if exists t_form_info;

drop table if exists t_permission;

drop table if exists t_resources;

drop table if exists t_role;

drop table if exists t_role_permission;

drop table if exists t_sheet_detail;

drop table if exists t_sheet_info;

drop table if exists t_sheet_process;

drop table if exists t_user;

drop table if exists t_user_depart;

drop table if exists t_user_role;

/*==============================================================*/
/* Table: t_department                                          */
/*==============================================================*/
create table t_department
(
   departId             varchar(25) not null comment '部门编号',
   name                 varchar(25) comment '部门名称',
   parentId             varchar(25) comment '父Id',
   description          varchar(200) comment '部门描述',
   resId                varchar(200) comment '部门图片资源Id',
   createTime           datetime comment '创建时间',
   phone                varchar(20) comment '部门手机',
   tel                  varchar(20) comment '部门座机',
   address              varchar(100) comment '部门地址',
   primary key (departId)
);

alter table t_department comment '部门表';

/*==============================================================*/
/* Table: t_form_detail                                         */
/*==============================================================*/
create table t_form_detail
(
   id                   varchar(50) not null,
   formid               varchar(50),
   content              text,
   sheetdetailid        varchar(30),
   primary key (id)
);

/*==============================================================*/
/* Table: t_form_info                                           */
/*==============================================================*/
create table t_form_info
(
   formId               varchar(50) not null,
   sheetId              varchar(50),
   createtime           time,
   deploymentId         varchar(50),
   userId               varchar(50),
   primary key (formId)
);

alter table t_form_info comment '表单信息表';

/*==============================================================*/
/* Table: t_permission                                          */
/*==============================================================*/
create table t_permission
(
   permissionId         varchar(50) not null comment '权限编号',
   name                 varchar(20) comment '权限名称：用户添加',
   permissionOrder      varchar(25) comment '权限标识指令：user:create代表用户添加权限',
   resourcePath         varchar(150) comment '权限资源地址',
   description          varchar(200) comment '权限描述',
   createTime           datetime comment '权限创建时间',
   resId                varchar(150) comment '权限图标资源Id',
   primary key (permissionId)
);

alter table t_permission comment '权限表';

/*==============================================================*/
/* Table: t_resources                                           */
/*==============================================================*/
create table t_resources
(
   resId                varchar(50),
   name                 varchar(50),
   description          varchar(100),
   path                 varchar(150),
   createTime           datetime,
   filename             varchar(50)
);

alter table t_resources comment '资源信息表';

/*==============================================================*/
/* Table: t_role                                                */
/*==============================================================*/
create table t_role
(
   roleId               varchar(50) not null,
   name                 varchar(50) comment '角色名称',
   description          varchar(200) comment '角色相关描述',
   roleSign             varchar(20) comment '角色标识：user',
   parentId             varchar(20) comment 'parentID',
   createTime           datetime comment '创建时间',
   resId                varchar(200) comment '角色标识',
   primary key (roleId)
);

alter table t_role comment '角色表';

/*==============================================================*/
/* Table: t_role_permission                                     */
/*==============================================================*/
create table t_role_permission
(
   role_permission_id   varchar(50) not null comment '角色权限关系ID',
   roleId               varchar(50),
   permissionId         varchar(50),
   primary key (role_permission_id)
);

alter table t_role_permission comment '角色权限关系表';

/*==============================================================*/
/* Table: t_sheet_detail                                        */
/*==============================================================*/
create table t_sheet_detail
(
   sheetDetailId        varchar(50) not null comment '表单属性ID',
   types                varchar(50) comment '控件类型，如：button,select,area,text等',
   labels               varchar(50) comment '文本信息',
   description          varchar(100) comment '描述',
   placeholder          varchar(100) comment '提示信息，针对Input控件',
   className            varchar(200) comment '样式名称：mui-title ',
   name                 varchar(200) comment '控件name',
   access               varchar(200) comment '是否限制角色显示',
   multiple             varchar(200),
   role                 varchar(200),
   list                 varchar(200) comment '下拉框中列表信息：[{label:"研发中心",vaklue:"yanfazhongxin"，select:"true"}]',
   sheetId              varchar(50),
   primary key (sheetDetailId)
);

alter table t_sheet_detail comment '表单属性配置表';

/*==============================================================*/
/* Table: t_sheet_info                                          */
/*==============================================================*/
create table t_sheet_info
(
   sheetId              varchar(20) not null comment '表单定义ID',
   name                 varchar(50) comment '表单名称，如：研发中心请假申请',
   types                varchar(50) comment '表单类型',
   description          varchar(150) comment '表单描述',
   createTime           date comment '创建时间',
   userId               varchar(50) comment '创建者',
   primary key (sheetId)
);

alter table t_sheet_info comment '表单定义表';

/*==============================================================*/
/* Table: t_sheet_process                                       */
/*==============================================================*/
create table t_sheet_process
(
   sheetProcessId       varchar(50) not null,
   sheetID              varchar(50),
   processId            varchar(50),
   primary key (sheetProcessId)
);

alter table t_sheet_process comment '表单流程关系表';

/*==============================================================*/
/* Table: t_user                                                */
/*==============================================================*/
create table t_user
(
   userId               varchar(50) not null,
   userName             varchar(50),
   passWord             varchar(50),
   phone                varchar(50) comment '电话',
   email                varchar(20),
   birthTime            date comment '出生日期',
   sex                  int comment '1:男，2:女',
   resId                varchar(100) comment '头像资源地址',
   address              varchar(150) comment '地址',
   state                varchar(20) comment '状态',
   dUserId              varchar(150) comment '叮叮员工id',
   workPlace            varchar(150) comment '办公地址',
   orderInDeparts       varchar(150) comment '在部门内部排序[{departId:index},...}]',
   jobNumber            varchar(50) comment '员工号',
   primary key (userId)
);

alter table t_user comment '用户表';

/*==============================================================*/
/* Table: t_user_depart                                         */
/*==============================================================*/
create table t_user_depart
(
   user_depart_id       varchar(30) not null,
   isLeader             varchar(10) comment '是否为部门负责人',
   userId               char(10),
   departId             char(10),
   primary key (user_depart_id)
);

alter table t_user_depart comment '用户部门关系表';

/*==============================================================*/
/* Table: t_user_role                                           */
/*==============================================================*/
create table t_user_role
(
   user_role_id         varchar(50) not null comment '用户角色关系ID',
   roleId               varchar(50),
   userId               varchar(50),
   primary key (user_role_id)
);

alter table t_user_role comment '用户角色表';

